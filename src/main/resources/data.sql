INSERT INTO USERS(name, email, password, created) VALUES('Roberto', 'roberto@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq',now());
INSERT INTO USERS(name, email, password, created) VALUES('Santos', 'santos@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq',now());

INSERT INTO PROFILE(name) VALUES('ROLE_ADMINISTRATOR');
INSERT INTO PROFILE(name) VALUES('ROLE_DEVELOPER');

INSERT INTO USERS_PROFILE(user_id, profile_id) VALUES(1, 1);
INSERT INTO USERS_PROFILE(user_id, profile_id) VALUES(2, 2);

INSERT INTO BOARD(name, owner_id, created) VALUES('Project JAVA', 1, now());
INSERT INTO BOARD(name, owner_id, created) VALUES('Project PYTHON', 1, now());
INSERT INTO BOARD(name, owner_id, created) VALUES('Project PHP', 2, now());

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new object java','Create a new object about cars','2022-06-07','2022-06-20',1,1,1,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new list java','Create a new list about cars','2022-06-07','2022-06-20',1,1,1,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new class java','Create a new class about cars','2022-06-07','2022-06-20',1,1,1,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new object python','Create a new object about cars','2022-06-07','2022-06-20',1,1,2,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new list python','Create a new list about cars','2022-06-07','2022-06-20',2,1,2,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new class python','Create a new class about cars','2022-06-07','2022-06-20',2,1,2,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new object php','Create a new object about cars','2022-06-07','2022-06-20',2,1,3,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new list php','Create a new list about cars','2022-06-07','2022-06-20',3,1,3,2);

INSERT INTO TASK(title,description,created,due_date,status,author_id,board_id,developer_id)
VALUES('Create new class php','Create a new class about cars','2022-06-07','2022-06-20',4,1,3,2);

INSERT INTO COMMENT(description,created,author_id,task_id)
VALUES('I`m working with Java ',now(),1,1);
INSERT INTO COMMENT(description,created,author_id,task_id)
VALUES('Today I will create a new list in PHP',now(),2,9);
INSERT INTO COMMENT(description,created,author_id,task_id)
VALUES('Today I will create a new object in PHP',now(),2,9);