package com.mytasks.mytasks.controller;

import com.mytasks.mytasks.dto.response.CommentResponseDTO;
import com.mytasks.mytasks.services.interfaces.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/tasks/comments")
    public Page<CommentResponseDTO> getAllComments(@PageableDefault(sort = "created", direction = Sort.Direction.DESC, page = 0, size = 20)
                                                   Pageable paginate) {
        return commentService.findAll(paginate);
    }

    @GetMapping("/tasks/{taskId}/comments")
    public Page<CommentResponseDTO> getAllCommentsByTaskId(@PathVariable(value = "taskId") Long taskId,
                                                           @PageableDefault(sort = "created", direction = Sort.Direction.DESC, page = 0, size = 20)
                                                Pageable paginate) {
        return commentService.findByTaskId(taskId, paginate);
    }


}
