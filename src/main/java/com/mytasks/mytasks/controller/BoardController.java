package com.mytasks.mytasks.controller;

import com.mytasks.mytasks.domain.Board;
import com.mytasks.mytasks.domain.User;
import com.mytasks.mytasks.dto.request.BoardRequestDTO;
import com.mytasks.mytasks.dto.response.BoardResponseDTO;
import com.mytasks.mytasks.dto.response.BoardResponseDetailsDTO;
import com.mytasks.mytasks.repositories.BoardRepository;
import com.mytasks.mytasks.services.interfaces.BoardService;
import com.mytasks.mytasks.services.UserService;
import com.mytasks.mytasks.util.ConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/boards")
public class BoardController {
    @Autowired
    private BoardService boardService;

    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private UserService userService;

    @Autowired
    private ConverterUtil converterUtil;

    @GetMapping
    public ResponseEntity<Page<BoardResponseDTO>> findPage(@RequestParam(required = false) String name,
                                       @PageableDefault(sort = "created", direction = Sort.Direction.DESC, page = 0, size = 24)
                              Pageable paginate) {
        Page<Board> pageBoards = (name == null) ? boardService.findAll(paginate) : boardService.findByName(name,paginate);
        Page<BoardResponseDTO> boards = converterUtil.toPageObjectDto(pageBoards);
        return ResponseEntity.ok().body(boards);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BoardResponseDetailsDTO> detail(@PathVariable Long id){
        Optional<Board> board = boardService.findById(id);
        Optional<BoardResponseDetailsDTO> boardDetail = converterUtil.toOptionalBoardDetailDto(board);
        return boardDetail.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity(HttpStatus.NO_CONTENT));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<BoardResponseDTO> save(@Valid @RequestBody BoardRequestDTO boardForm){
        Board board = converterUtil.toBoard(boardForm);
        Optional<User> user = userService.findById(boardForm.getOwner());
        board.setOwner(user.orElseThrow());
        BoardResponseDTO boardResponseDTO = converterUtil.toBoardDto(boardService.save(board));
        return ResponseEntity.status(HttpStatus.CREATED).body(boardResponseDTO);
    }
}
