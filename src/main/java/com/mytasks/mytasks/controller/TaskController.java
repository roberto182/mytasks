package com.mytasks.mytasks.controller;

import com.mytasks.mytasks.dto.response.TaskResponseDTO;
import com.mytasks.mytasks.services.interfaces.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping
    public Page<TaskResponseDTO> list(@RequestParam(required = false) String title,
                                      @PageableDefault(sort = "created", direction = Sort.Direction.DESC, page = 0, size = 20)
                                      Pageable paginate) {
        if(title == null){
            return taskService.findAll(paginate);
        }
        return taskService.findByTitle(title,paginate);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskResponseDTO> detail(@PathVariable Long id){
        Optional<TaskResponseDTO> task = taskService.findById(id);
        return task.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity(HttpStatus.NO_CONTENT));
    }
}
