package com.mytasks.mytasks.services.implementation;

import com.mytasks.mytasks.mapper.UserMapper;
import com.mytasks.mytasks.repositories.UsersRepository;
import com.mytasks.mytasks.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    UserMapper userMapper;

//    @Override
//    public Optional<UserResponseDTO> findById(Long id) {
//        Optional<User> user = usersRepository.findById(id);
//        return userMapper.userToUserResponseDTO(user);
//    }

}
