package com.mytasks.mytasks.services.implementation;

import com.mytasks.mytasks.dto.response.TaskResponseDTO;
import com.mytasks.mytasks.domain.Task;
import com.mytasks.mytasks.mapper.TaskMapper;
import com.mytasks.mytasks.repositories.TaskRepository;
import com.mytasks.mytasks.services.interfaces.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    TaskRepository taskRepository;
    @Autowired
    TaskMapper taskMapper;

    @Override
    public Page<TaskResponseDTO> findAll(Pageable paginate){
        Page<Task> task = taskRepository.findAll(paginate);
        return taskMapper.toPageResponseDTO(task);
    }

    @Override
    public Page<TaskResponseDTO> findByTitle(String name, Pageable paginate){
        Page<Task> task = taskRepository.findByTitle(name,paginate);
        return taskMapper.toPageResponseDTO(task);
    }

    @Override
    public Optional<TaskResponseDTO> findById(Long id) {
        Optional<Task> task = taskRepository.findById(id);
        if (task.isPresent()){
            return taskMapper.toOptionalResponseDTO(task);
        }
        return Optional.empty();
    }
}
