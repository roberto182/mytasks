package com.mytasks.mytasks.services.implementation;

import com.mytasks.mytasks.domain.Board;
import com.mytasks.mytasks.domain.User;
import com.mytasks.mytasks.dto.request.BoardRequestDTO;
import com.mytasks.mytasks.dto.response.BoardResponseDTO;

import com.mytasks.mytasks.mapper.UserMapper;
import com.mytasks.mytasks.repositories.BoardRepository;
import com.mytasks.mytasks.services.UserService;
import com.mytasks.mytasks.services.interfaces.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class BoardServiceImpl implements BoardService {
    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserService userService;

    @Override
    public Page<Board> findAll(Pageable paginate){
        return boardRepository.findAll(paginate);//boardMapper.toPageResponseDTO(board);
    }

    @Override
    public Page<Board> findByName(String name, Pageable paginate){
        return boardRepository.findByName(name,paginate);
    }

    @Override
    public Optional<Board> findById(Long id) {
        return boardRepository.findById(id);
    }

    @Override
    @Transactional
    public Board save(Board board) {
        board.setId(null);
        board = boardRepository.save(board);
        return board;
    }


//    @Override
//    public Optional<BoardResponseDetailsDTO> findById(Long id) {
//        Optional<Board> board = boardRepository.findById(id);
//
//        return Optional.ofNullable(boardMapper.toBoardResponseDetailsDTO(board.get()));
//
//    }
//
//    private BoardResponseDetailsDTO returnIfBoardExist(Long id){
//
//        Optional<Board> board = boardRepository.findById(id);
//
//        return boardMapper.toBoard(board);
//    }

//
//    public Optional<Board> findById(Long id) {
//        return boardRepository.findById(id);
//    }
//

}
