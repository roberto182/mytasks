package com.mytasks.mytasks.services.implementation;

import com.mytasks.mytasks.dto.response.CommentResponseDTO;
import com.mytasks.mytasks.domain.Comment;
import com.mytasks.mytasks.mapper.CommentMapper;
import com.mytasks.mytasks.repositories.CommentRepository;
import com.mytasks.mytasks.services.interfaces.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    CommentMapper commentMapper;


    @Override
    public Page<CommentResponseDTO> findAll(Pageable paginate) {
        Page<Comment> comments = commentRepository.findAll(paginate);
        return commentMapper.toPageResponseDTO(comments);
    }

    @Override
    public Optional<CommentResponseDTO> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Page<CommentResponseDTO> findByTaskId(Long taskId, Pageable paginate) {
        Page<Comment> comments = commentRepository.findByTaskId(taskId, paginate);
        return commentMapper.toPageResponseDTO(comments);
    }

//    @Override
//    public Page<CommentResponseDTO> findByAuthor(String author, Pageable paginate) {
//        Page<Comment> board = commentRepository.findByAuthor(author,paginate);
//        return commentMapper.toPageResponseDTO(board);
//    }

    @Override
    public Optional<CommentResponseDTO> findByIdAndTaskId(Long id, Long taskId) {
        return Optional.empty();
    }
}
