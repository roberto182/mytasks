package com.mytasks.mytasks.services.interfaces;

import com.mytasks.mytasks.domain.Board;
import com.mytasks.mytasks.dto.request.BoardRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface BoardService {

    Page<Board> findAll(Pageable pageable);

    Page<Board> findByName(String name, Pageable pageable);

    Optional<Board> findById(Long id);

    Board save(Board board);
}
