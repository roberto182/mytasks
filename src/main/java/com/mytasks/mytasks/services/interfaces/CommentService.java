package com.mytasks.mytasks.services.interfaces;

import com.mytasks.mytasks.dto.response.CommentResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CommentService {

    Page<CommentResponseDTO> findAll(Pageable paginate);
    Optional<CommentResponseDTO> findById(Long id);
    Page<CommentResponseDTO> findByTaskId(Long taskId, Pageable paginate);
//    Page<CommentResponseDTO> findByAuthor(String author, Pageable paginate);
    Optional<CommentResponseDTO> findByIdAndTaskId(Long id, Long taskId);
}
