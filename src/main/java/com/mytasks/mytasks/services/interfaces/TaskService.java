package com.mytasks.mytasks.services.interfaces;

import com.mytasks.mytasks.dto.response.TaskResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface TaskService {
    Page<TaskResponseDTO> findAll(Pageable pageable);

    Page<TaskResponseDTO> findByTitle(String name, Pageable pageable);

    Optional<TaskResponseDTO> findById(Long id);

//    public Page<Task> findAll(Pageable paginate){
//        return taskRepository.findAll(paginate);
//    }
//
//    public Optional<Task> findById(Long id) {
//        return taskRepository.findById(id);
//    }
//
//    public Page<Task> findByTitle(String title, Pageable paginate) {
//        return taskRepository.findByTitle(title,paginate);
//    }
}
