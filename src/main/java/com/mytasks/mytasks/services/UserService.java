package com.mytasks.mytasks.services;

import com.mytasks.mytasks.domain.User;
import com.mytasks.mytasks.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UsersRepository usersRepository;

    public Optional<User> findById(Long id){
        return usersRepository.findById(id);
    }
    public Page<User> findByEmail(String email, Pageable paginate){
        return usersRepository.findByEmail(email,paginate);
    }
    public Page<User> findAll(Pageable paginate){
        return usersRepository.findAll(paginate);
    }
}
