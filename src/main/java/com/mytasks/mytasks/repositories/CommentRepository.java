package com.mytasks.mytasks.repositories;

import com.mytasks.mytasks.domain.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findAll(Pageable pageable);
    Optional<Comment> findById(Long id);
    Page<Comment> findByTaskId(Long taskId, Pageable paginate);
    Optional<Comment> findByIdAndTaskId(Long id, Long taskId);
//    Page<Comment> findByAuthor(String author, Pageable paginate);
}
