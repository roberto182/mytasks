package com.mytasks.mytasks.repositories;

import com.mytasks.mytasks.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    Page<User> findByEmail(String email, Pageable paginate);
    Page<User> findAll(Pageable paginate);
    Optional<User> findById(Long id);
}
