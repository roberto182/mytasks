package com.mytasks.mytasks.repositories;

import com.mytasks.mytasks.domain.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface TaskRepository extends JpaRepository<Task, Long> {
    Page<Task> findByTitle(String name, Pageable paginate);
    Page<Task> findAll(Pageable paginate);

    Optional<Task> findById(Long id);
}
