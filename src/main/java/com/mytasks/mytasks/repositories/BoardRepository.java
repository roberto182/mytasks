package com.mytasks.mytasks.repositories;

import com.mytasks.mytasks.domain.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BoardRepository extends JpaRepository<Board, Long> {
    Page<Board> findByName(String name, Pageable paginate);
    Page<Board> findAll(Pageable paginate);
    Optional<Board> findById(Long id);

    Board save(Board board);

}
