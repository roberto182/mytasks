package com.mytasks.mytasks.util;

import com.mytasks.mytasks.domain.Board;
import com.mytasks.mytasks.dto.request.BoardRequestDTO;
import com.mytasks.mytasks.dto.response.BoardResponseDTO;
import com.mytasks.mytasks.dto.response.BoardResponseDetailsDTO;
import com.mytasks.mytasks.dto.response.TaskResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConverterUtil {
    public Page<BoardResponseDTO> toPageObjectDto(Page<Board> objects) {
        return objects.map(this::convertToObjectDto);
    }
    private BoardResponseDTO convertToObjectDto(Board board) {
        BoardResponseDTO dto = new BoardResponseDTO();
        dto.setId(board.getId());
        dto.setCreated(board.getCreated());
        dto.setName(board.getName());
        dto.setOwner(board.getOwner().getName());
        return dto;
    }

    public Optional<BoardResponseDetailsDTO> toOptionalBoardDetailDto(Optional<Board> objects) {
        return objects.map(this::convertToBoardDetailDto);
    }
    private BoardResponseDetailsDTO convertToBoardDetailDto(Board board) {
        BoardResponseDetailsDTO dto = new BoardResponseDetailsDTO();
        dto.setId(board.getId());
        dto.setCreated(board.getCreated());
        dto.setName(board.getName());
        dto.setOwner(board.getOwner().getName());
        dto.setTasks(board.getTasks().stream().map(TaskResponseDTO::new).toList());
        return dto;
    }

    public Board toBoard(BoardRequestDTO boardDto) {
        Board board = new Board();
        board.setName(boardDto.getName());
        return board;
    }

    public BoardResponseDTO toBoardDto(Board board) {
        BoardResponseDTO dto = new BoardResponseDTO();
        dto.setId(board.getId());
        dto.setCreated(board.getCreated());
        dto.setName(board.getName());
        dto.setOwner(board.getOwner().getName());
        return dto;
    }
}
