package com.mytasks.mytasks.domain.enums;

public enum StatusTask {
    BACKLOG(1,"Backlog"),
    TO_DO(2,"To Do"),
    IN_PROGRESS(3, "In Progress"),
    DONE(4,"Done");

    private int cod;
    private String description;

    StatusTask(int cod, String description) {
        this.cod = cod;
        this.description = description;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public  static StatusTask toEnum(Integer cod){
        if(cod == null){
            return null;
        }
        for (StatusTask x : StatusTask.values()){
            if(cod.equals(x.getCod())){
                return x;
            }
        }
        throw new IllegalArgumentException("Id invalid: "+cod);
    }
}
