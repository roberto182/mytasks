package com.mytasks.mytasks.mapper;

import com.mytasks.mytasks.dto.response.TaskResponseDTO;
import com.mytasks.mytasks.domain.Task;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    default Page<TaskResponseDTO> toPageResponseDTO(Page<Task> tasks){
        return tasks.map(TaskResponseDTO::new);
    }

    default Optional<TaskResponseDTO> toOptionalResponseDTO(Optional<Task> task){
        return task.map(TaskResponseDTO::new);
    }

}
