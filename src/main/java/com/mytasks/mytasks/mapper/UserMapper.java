package com.mytasks.mytasks.mapper;

import com.mytasks.mytasks.dto.response.UserResponseDTO;
import com.mytasks.mytasks.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    default Page<UserResponseDTO> toPageResponseDTO(Page<User> users){
            return users.map(UserResponseDTO::new);
    }

//    Optional<UserResponseDTO> userToUserResponseDTO(Optional<User> user);
}
