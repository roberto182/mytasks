package com.mytasks.mytasks.mapper;

import com.mytasks.mytasks.dto.response.CommentResponseDTO;
import com.mytasks.mytasks.domain.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    default Page<CommentResponseDTO> toPageResponseDTO(Page<Comment> comments){
            return comments.map(CommentResponseDTO::new);
    }

    default Optional<CommentResponseDTO> toOptionalResponseDTO(Optional<Comment> comment){
            return comment.map(CommentResponseDTO::new);
    }
}
