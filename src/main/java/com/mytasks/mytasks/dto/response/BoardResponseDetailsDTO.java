package com.mytasks.mytasks.dto.response;

import com.mytasks.mytasks.domain.Board;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BoardResponseDetailsDTO {
    private Long id;
    private String name;
    private LocalDateTime created;
    private String owner;
    private List<TaskResponseDTO> tasks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<TaskResponseDTO> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskResponseDTO> tasks) {
        this.tasks = tasks;
    }
}
