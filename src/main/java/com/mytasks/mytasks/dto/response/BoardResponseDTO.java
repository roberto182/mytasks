package com.mytasks.mytasks.dto.response;

import com.mytasks.mytasks.domain.*;

import java.time.LocalDateTime;

public class BoardResponseDTO {
    private Long id;
    private String name;
    private LocalDateTime created;
    private String owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
