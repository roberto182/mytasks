package com.mytasks.mytasks.dto.response;

import com.mytasks.mytasks.domain.Comment;

import java.time.LocalDateTime;

public class CommentResponseDTO {
    private Long id;
    private String description;
    private LocalDateTime created;
    private String author;

    public CommentResponseDTO(Comment comment) {
        this.id = comment.getId();
        this.description = comment.getDescription();
        this.created = comment.getCreated();
        this.author = comment.getAuthor().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}
