package com.mytasks.mytasks.dto.response;

import com.mytasks.mytasks.domain.*;
import com.mytasks.mytasks.domain.enums.StatusTask;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskResponseDTO {
    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String description;
    private StatusTask status;
    @NotBlank
    private Date dueDate;
    private LocalDateTime created;
    @NotBlank
    private String author;
    @NotBlank
    private String developer;

    public TaskResponseDTO(Task task) {
        this.id = task.getId();
        this.title = task.getTitle();
        this.description = task.getDescription();
        this.status = task.getStatus();
        this.dueDate = task.getDueDate();
        this.created = task.getCreated();
        this.author = task.getAuthor().getName();
        this.developer = task.getDeveloper().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StatusTask getStatus() {
        return status;
    }

    public void setStatus(StatusTask status) {
        this.status = status;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

//    public static Page<TaskDto> convert(Page<Task> tasks) {
//        return tasks.map(TaskDto::new);
//    }
}
