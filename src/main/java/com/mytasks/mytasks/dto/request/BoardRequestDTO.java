package com.mytasks.mytasks.dto.request;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;

public class BoardRequestDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @NotNull
    @NotEmpty(message = "Name is required")
    @Length(min = 3, max = 100)
    private String name;

    @NotNull
    private Long owner;

    public BoardRequestDTO(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

}
